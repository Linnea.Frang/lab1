package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    List<String> yesOrNo = Arrays.asList("y", "n");
    
    public void run() {
    	while (true) {
    		System.out.println("Let's play round "+roundCounter);
    		String humanChoice = userChoice();
    		String computerChoice = randomChoice();
    		System.out.printf("Human chose "+humanChoice+ ", computer chose "+computerChoice+". ");
    		
    		if (isWinner(humanChoice, computerChoice)) {
    			System.out.println("Human wins!");
    			humanScore ++;
    		}
    		else if (isWinner(computerChoice, humanChoice)) {
    			System.out.println("Computer wins!");
    			computerScore ++;
    		}
    		else {
    			System.out.println("It's a tie!");
    		}
    		System.out.println("Score: human "+humanScore+", computer "+computerScore);
    		
    		String continueAnswer = continuePlaying();
    		if (continueAnswer.equals("n")) {
    			break;
    		}
    		roundCounter ++;
    	}
    	System.out.println("Bye bye :)");
    }
    
    private String continuePlaying() {
		while (true) {
			String continueAnswer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
			if (validateInput(continueAnswer, yesOrNo)) {
				return continueAnswer;
			}
			else {
				System.out.println("I don't understand "+continueAnswer+". Try again");
			}		
		}
	}

	private boolean validateInput(String continueAnswer, List<String> validInput) {
		return validInput.contains(continueAnswer);
	}

	/**
     * Tester hvem som vinner runden
     * @param choice1
     * @param choice2
     * @return
     */
    private boolean isWinner(String choice1, String choice2) {
    	if (choice1.equals("paper")) {
    		return choice2.equals("rock");
    	}
    	else if (choice1.equals("scissors")) {
    		return choice2.equals("paper");
    	}
    	else {
    		return choice2.equals("scissors");
    	}
	}


	private String randomChoice() {
		Random randInt = new Random();
		int randomIndex = randInt.nextInt(2);
		return rpsChoices.get(randomIndex);
	}



	private String userChoice() {
		while (true) {
			String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
			if (validateInput(humanChoice, rpsChoices)) {
				return humanChoice;	
			}
			else {
				System.out.println("I don't understand "+humanChoice+". Try again");
			}
		}
	}


	/**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
